/*
 *           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                       Version 2, December 2004
 *
 *    Copyright (C) 2020 Henrique Dante de Almeida
 *
 *    Everyone is permitted to copy and distribute verbatim or modified
 *    copies of this license document, and changing it is allowed as long
 *    as the name is changed.
 *
 *               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *     0. You just DO WHAT THE FUCK YOU WANT TO.
*/

#ifndef DEFS_H
#define DEFS_H

#ifndef HAVE_ISO_C
#if defined(__STDC__) || defined(__STDC_VERSION__)
#define HAVE_ISO_C 1
#endif
#endif /* HAVE_ISO_C */

#ifdef __STDC_VERSION__
#ifndef HAVE_C94
#if __STDC_VERSION__ >= 199409L
#define HAVE_C94 1
#endif
#endif /* HAVE_C94 */

#ifndef HAVE_C99
#if __STDC_VERSION__ >= 199901L
#define HAVE_C99 1
#endif
#endif /* HAVE_C99 */

#ifndef HAVE_C11
#if __STDC_VERSION__ >= 201112L
#define HAVE_C11 1
#endif
#endif /* HAVE_C11 */

#ifndef HAVE_C18
#if __STDC_VERSION__ >= 201710L
#define HAVE_C18 1
#endif
#endif /* HAVE_C18 */
#endif /* __STDC_VERSION__ */

#ifdef __cplusplus
#ifndef HAVE_CXX
#define HAVE_CXX 1
#endif /* HAVE_CXX */

#ifndef HAVE_CXX98
#if __cplusplus >= 199711L
#define HAVE_CXX98 1
#endif
#endif /* HAVE_CXX98 */

#ifndef HAVE_CXX11
#if __cplusplus >= 201103L
#define HAVE_CXX11 1
#endif
#endif /* HAVE_CXX11 */

#ifndef HAVE_CXX14
#if __cplusplus >= 201402L
#define HAVE_CXX14 1
#endif
#endif /* HAVE_CXX14 */

#ifndef HAVE_CXX17
#if __cplusplus >= 201703L
#define HAVE_CXX17 1
#endif
#endif /* HAVE_CXX17 */
#endif /* __cplusplus */

#ifndef HAVE_UNIX
#if defined(unix) || defined(__unix) || defined(__unix__)
#define HAVE_UNIX 1
#endif
#endif /* HAVE_UNIX */

#ifndef HAVE_WINDOWS
#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#define HAVE_WINDOWS 1
#endif
#endif /* HAVE_WINDOWS */

#ifdef __MACH__
#ifndef HAVE_MACH
#define HAVE_MACH 1
#endif /* HAVE_MACH */
#ifdef __APPLE__
#ifndef HAVE_APPLE
#define HAVE_APPLE 1
#endif /* HAVE_APPLE */
#endif /* __APPLE */
#endif /* __MACH__ */

#ifndef HAVE_BSD
#if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) \
	|| defined(__bsdi__) || defined(__DragonFly__) || defined(__APPLE__) \
	|| defined(BSD) || defined(_SYSTYPE_BSD)
#define HAVE_BSD 1
#endif
#endif /* HAVE_BSD */

#ifndef HAVE_AMD64
#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) \
	|| defined(__x86_64) || defined(_M_AMD64)
#define HAVE_AMD64 1
#endif
#endif /* HAVE_AMD64 */

#ifndef HAVE_ARM
#if defined(__arm__) || defined(__thumb__) || defined(_ARM) \
	|| defined(_M_ARM) || defined(_M_ARMT) || defined(__arm)
#define HAVE_ARM 1
#endif
#endif /* HAVE_ARM */

#ifndef HAVE_ARM64
#ifdef __aarch64__
#define HAVE_ARM64 1
#endif
#endif /* HAVE_ARM64 */

#ifndef HAVE_IA32
#if defined(i386) || defined(__i386) || defined(__i386__) \
	|| defined(__i486__) || defined(__i586__) || defined(__i686__) \
	|| defined(__IA32__) || defined(_M_I86) || defined(_M_IX86) \
	|| defined(__X86__) || defined(_X86_) || defined(__THW_INTEL__) \
	|| defined(__I86__) || defined(__INTEL__) || defined(__386)
#define HAVE_IA32 1
#endif
#endif /* HAVE_IA32 */

#ifndef HAVE_MIPS
#if defined(__mips__) || defined(mips) || defined(__mips) || defined(__MIPS__)
#define HAVE_MIPS 1
#endif
#endif /* HAVE_MIPS */

#ifndef HAVE_POWERPC
#if defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) \
	|| defined(__POWERPC__) || defined(__ppc__) || defined(__ppc64__) \
	|| defined(__PPC__) || defined(__PPC64__) || defined(_ARCH_PPC) \
	|| defined(_ARCH_PPC64) || defined(_M_PPC) || defined(__ppc)
#define HAVE_POWERPC 1
#endif
#endif /* HAVE_POWERPC */

#ifndef HAVE_LINUX
#if defined(linux) || defined(__linux) || defined(__linux__)
#define HAVE_LINUX 1
#endif
#endif /* HAVE_LINUX */

#ifndef HAVE_FREEBSD
#ifdef __FreeBSD__
#define HAVE_FREEBSD 1
#endif
#endif /* HAVE_FREEBSD */

#ifndef HAVE_NETBSD
#ifdef __NetBSD__
#define HAVE_NETBSD 1
#endif
#endif /* HAVE_NETBSD */

#ifndef HAVE_OPENBSD
#ifdef __OpenBSD__
#define HAVE_OPENBSD 1
#endif
#endif /* HAVE_OPENBSD */

#ifndef HAVE_DRAGONFLYBSD
#ifdef __DragonFly__
#define HAVE_DRAGONFLYBSD 1
#endif
#endif /* HAVE_DRAGONFLYBSD */

#ifndef HAVE_GNU_LINUX
#ifdef __gnu_linux__
#define HAVE_GNU_LINUX 1
#endif
#endif /* HAVE_GNU_LINUX */

#ifndef HAVE_ANDROID
#ifdef __ANDROID__
#define HAVE_ANDROID 1
#endif
#endif /* HAVE_ANDROID */

#ifndef HAVE_CYGWIN
#ifdef __CYGWIN__
#define HAVE_CYGWIN 1
#endif
#endif /* HAVE_CYGWIN */

#ifndef HAVE_MSYS
#ifdef __MSYS__
#define HAVE_MSYS 1
#endif
#endif /* HAVE_MSYS */

#ifndef HAVE_GCC
#ifdef __GNUC__
#define HAVE_GCC 1
#endif
#endif /* HAVE_GCC */

#ifndef HAVE_LLVM
#ifdef __llvm__
#define HAVE_LLVM 1
#endif
#endif /* HAVE_LLVM */

#ifndef HAVE_CLANG
#ifdef __clang__
#define HAVE_CLANG 1
#endif
#endif /* HAVE_CLANG */

#ifndef HAVE_MSC
#ifdef _MSC_VER
#define HAVE_MSC 1
#endif
#endif /* HAVE_MSC */

#ifndef HAVE_MINGW
#if defined(__MINGW32__) || defined(__MINGW64__)
#define HAVE_MINGW 1
#endif
#endif /* HAVE_MINGW */

#ifdef HAVE_ISO_C
#include <stddef.h>
#include <limits.h>
#endif

#if defined(HAVE_C99) || defined(HAVE_CXX11)
#include <stdbool.h>
#include <stdint.h>
#endif

#ifdef HAVE_CXX
#include <cstddef>
#endif

#ifdef HAVE_UNIX
#include <sys/types.h>
#include <unistd.h>
#endif

#ifdef HAVE_WINDOWS
#include <windows.h>
#endif

#ifndef HAVE_GLIBC
#if defined(__GLIBC__) || defined(__GNU_LIBRARY__)
#define HAVE_GLIBC 1
#endif
#endif /* HAVE_GLIBC */

#ifndef HAVE_BIONIC
#ifdef __BIONIC__
#define HAVE_BIONIC 1
#endif
#endif /* HAVE_BIONIC */

#ifndef HAVE_UCLIBC
#ifdef __UCLIBC__
#define HAVE_UCLIBC 1
#endif
#endif /* HAVE_UCLIBC */

#ifndef HAVE_LIBSTDCXX
#if defined(__GLIBCPP__) || defined(__GLIBCXX__)
#define HAVE_LIBSTDCXX 1
#endif
#endif /* HAVE_LIBSTDCXX */

#ifndef HAVE_LIBCXX
#ifdef _LIBCPP_VERSION
#define HAVE_LIBCXX 1
#endif
#endif /* HAVE_LIBCXX */

#ifndef HAVE_POSIX
#if defined(_POSIX_VERSION) || defined(_POSIX2_C_VERSION)
#define HAVE_POSIX 1
#endif
#endif /* HAVE_POSIX */

#ifndef HAVE_XOPEN
#ifdef _XOPEN_VERSION
#define HAVE_XOPEN 1
#endif
#endif /* HAVE_XOPEN */

#ifndef HAVE_CXX
#ifndef max
#define max(x, y) ((x) > (y) ? (x) : (y))
#endif /* max */

#ifndef min
#define min(x, y) ((x) < (y) ? (x) : (y))
#endif /* min */
#endif /* HAVE_CXX */

#ifndef RESTRICT
#if HAVE_C99
#define RESTRICT restrict
#else
#define RESTRICT
#endif
#endif /* RESTRICT */

#ifndef countof
#define countof(s) (sizeof(s)/sizeof((s)[0]))
#endif /* countof */

#endif /* DEFS_H */
